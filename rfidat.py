#!/usr/bin/env python
import socket, ConfigParser, time, logs, sys, os, threading, json, ast
import paho.mqtt.client as mqtt

### Get run mode
if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'

logger = logs.setup_logger('rfidat')
logger.warning("Program Started...")
starttime=time.time()

### Functions start here

def network_check():
	import socket
	loop = 1
        while loop == 1:
		try:
			host = socket.gethostbyname(config['mqtt_broker_hostname'])
 			s = socket.create_connection((host, config['mqtt_broker_port']), 2)
			logger.warning("Network checks passed!, '%s' is reachable." % config['mqtt_broker_hostname'])
			loop = 0
		except Exception:
			logger.warning("Network checks failed!, stalling until MQTT Broker '%s' becomes reachable..." % config['mqtt_broker_hostname'])
			time.sleep(5)

### Load config parameters
def config_load():
	global config
	global sockets
	config = {}
	config_file = ConfigParser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'hostid':config_file.get('host', 'id'),
	'mqtt_broker_hostname':config_file.get('mqtt_broker', 'hostname'),
	'mqtt_broker_port':config_file.getint('mqtt_broker', 'port'),
	'mqtt_broker_username':config_file.get('mqtt_broker', 'username'),
	'mqtt_broker_pass':config_file.get('mqtt_broker', 'password'),
	'mqtt_publish_topic':config_file.get('mqtt_broker', 'publish_topic'),
	'mqtt_subscribe_topic':config_file.get('mqtt_broker', 'subscribe_topic'),
	'indoor_locations':config_file.get('locations', 'indoors').split(','),
	'outdoor_locations':config_file.get('locations', 'outdoors').split(',')
	})

### Load radio_db file and settings 
def rfidtag_db_load():
	global rfidtag_db
	rfidtag_db = ConfigParser.RawConfigParser()
	rfidtag_db.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'rfidtag_db.cfg'))

### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
		client.subscribe(config['mqtt_subscribe_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % s)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH 'm%s' completed." % mid)

def on_message(client, userdata, msg):
	if msg.topic == config['mqtt_subscribe_topic']:
		logger.info("Processing Publish '%s'." % msg.payload)
		# convert payload to a dictionary 
		# we should measure the lenght of payload, count items and check if we counted them all before passing THE DECIDER 
		payload = ast.literal_eval(msg.payload)
		for key, value in payload.iteritems():
			if key == 'tag' and 'track_event' not in locals():
				track_event = {}
				# we've got something, now we need to inspect the whole message again
				for key, value in payload.iteritems():
					if key == 'tag':
						track_event.update({'tag':value})
					elif key == 'location':
						track_event.update({'location':value})
					elif key == 'sublocation':
						track_event.update({'sublocation':value})
					elif key == 'antenna':
						track_event.update({'antenna':value})
					elif key == 'reader':
						track_event.update({'reader':value})
					else:
						continue
				# now we push the message to the learner 
				logger.info("Received an MQTT message with the following '%s', pushing message to the learner for further processing..." % track_event)
				#learner = the_learner('presence', kwargs=event)
				#learner.start()
				# get name of running threads, if we are already processing an event for this location we dont push it to the decider
				#thread_name = str(event['sublocation'])+"_motion"
				#running_threads = get_threads()
				#if thread_name in running_threads:
				#	logger.info("A thread for this event is still running: '%s'. Refreshing timers." % thread_name)
				#	add_time(thread_name)
				#else:
				#	#push this event the decider
				#	logger.info("Received an MQTT message with the following '%s', pushing message to the decider for further processing..." % track_event)
				#	decider  = the_decider(name=thread_name, args=(1,), kwargs=event)
				#s	decider.start()
				logger.info("Received an MQTT message with the following '%s', pushing message to the decider for further processing..." % track_event)
				thread  = track_it(track_event)
				thread.start()

def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client(config['hostid'])
		mqttc.username_pw_set(config['mqtt_broker_pass'], config['mqtt_broker_pass'])
		mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
		mqttc.on_connect = on_connect
		mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.on_log = on_log
		mqttc.loop_start()

class track_it(threading.Thread):
	def __init__(self, track_event):
		threading.Thread.__init__(self)
		self.rfidtag_db = rfidtag_db
		self.track_event = track_event
		return
	def run(self):
		try:
			logger.debug("track_it received the following tracking event '%s'" % self.track_event)
			for key, value in self.track_event.iteritems():
				if key == 'tag':
					tagid = value
					if not self.rfidtag_db.has_option(tagid, 'aid'):
						logger.exception("Missing animal declaration in rfidtag_db config file!, offending tag id: '%s'" % tagid)
						return
				elif key == 'location':
					location = value
				elif key == 'sublocation':
					sublocation = value
				elif key == 'antenna':
					antenna = value
				elif key == 'reader':
					reader = value
			
			aid = self.rfidtag_db.get(tagid,'aid')
			name = self.rfidtag_db.get(tagid,'name')
			status = self.rfidtag_db.get(tagid, 'status')
			print "i see '%s', id '%s' and she's '%s'" % (name, aid, status)

		except Exception:
			sys.excepthook(*sys.exc_info())

if __name__ == "__main__":
	print "Rfidat is spinning. Check /var/log/rfidat.log for more details..."
	# Load config from file config.cfg
	config_load()
	# Perfrom network checks and wait there until we can reach the MQTT broker
	network_check()
	# Load rfid tag db
	rfidtag_db_load()
	try:
		# Start MQTT thread here 
		thread0 = mqtt_connect()
		thread0.daemon = True
		thread0.start()
		logger.warning("Started mqtt_connect thread...")
		if runmode == 'debug':
			print "Started mqtt_connect thread..."
		# Start socket listener threads here

	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print "Got exception on main handler:"
		mqttc.loop_stop()
		mqttc.disconnect()

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print "Other error or exception occurred!"
		mqttc.loop_stop()
		mqttc.disconnect()

	while True:
		time.sleep(1)
		pass

					
